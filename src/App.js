import React,{ lazy, Suspense } from 'react'
import './App.scss';
import {BrowserRouter as Router,Switch,Route} from "react-router-dom";
import Loader from './components/Loader/Loader';
const Home = lazy(() => import('./pages/Home'))
const Mall = lazy(() => import('./pages/Mall'))
const Shop = lazy(() => import('./pages/Shop'))
const Article = lazy(() => import('./pages/Article'))



function App() {
  return (
    <div className="app">
      <Suspense fallback={ <Loader/>}>
<Router>
<Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/mall/:id" component={Mall} />
      <Route exact path="/shop/:id" component={Shop} />
      <Route exact path="/article/:id" component={Article} />
    </Switch>
    </Router>
    </Suspense>
    </div>
  );
}

export default App;
