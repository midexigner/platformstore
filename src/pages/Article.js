import React from 'react'
import Banner from '../components/Banner/Banner'
import Navbar from '../components/Navbar/Navbar'
import ProductCard from '../components/ProductCard/ProductCard'

const Article = () => {
    return (
        <>
          <Navbar />
            <Banner title="Single Article" image='https://picsum.photos/1700/760'/>
           <div className="container">
               <div className="row">
               <ProductCard isDetailed title="store Mall" desc="London is the capital and largest city of England and the United Kingdom. The city stands on the River Thames in the south-east of England " img="https://picsum.photos/400/450" />
               </div>

           </div>
        </>
    )
}

export default Article