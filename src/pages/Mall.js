import React from 'react'
import Banner from '../components/Banner/Banner'
import ProductCard from '../components/ProductCard/ProductCard'
import Title from '../components/Title/Title'
import { FiSearch } from "react-icons/fi";
import { useStateValue } from '../StateProvider';
import Navbar from '../components/Navbar/Navbar';

const Mall = () => {
  const [data, dispatch] = useStateValue();
  const searchMall = (event) =>{
    let keyword = event.target.value;
     dispatch({
         type:'search',
         search:keyword,
        })
  }
    return (
        <>
          <Navbar />
           <Banner title="Search in Mall" image='https://picsum.photos/1700/760'>
            <form className="form-inline search w-100">
            <div className="input-group mb-3 w-100">   
            <input type="search" className="form-control searchInput" placeholder="Search in Mall" onChange={(e)=>searchMall(e)} />
    <div className="input-group-append searchBtn"><FiSearch/></div>
  </div>
                </form>
            </Banner>
            <section>
            <div className="container py-md-5">
            <Title title="All Mall " subtitle="visit" />
            <div className="row">
     {data.mall.filter(function (item) {
           if(data.search == null)
          return item
      else if(item.name.toLowerCase().includes(data.search.toLowerCase())){
          return item
      }
           }).map((item)=>(<ProductCard key={item.name} title={item.name} url={`/shop/${item.name}`} img={item.image} />))}
            </div>
            </div>
        </section> 
        </>
    )
}

export default Mall