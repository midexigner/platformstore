import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import reducer,{initialState} from './Reducer'
import { StateProvider } from './StateProvider';


ReactDOM.render(
  <React.StrictMode>
    <StateProvider reducer={reducer} initialState={initialState}>
    <App />
    </StateProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
/* 
https://freetutorials-us.com/the-complete-flutter-development-bootcamp-with-dart-2/
https://freetutorials-us.com/flutter-dart-the-complete-flutter-app-development-course/
https://freetutorials-us.com/flutter-advanced-course/
https://freetutorials-us.com/flutter-beginners-course/
https://freetutorials-us.com/the-complete-2020-flutter-development-bootcamp-with-dart/
https://freetutorials-us.com/the-complete-2020-flutter-development-bootcamp-with-dart-2/
https://freetutorials-us.com/mern-ecommerce-from-scratch/

*/
