export const initialState = {
    search:null,
    cities:[
        {'name':'Paris','image':'https://www.austrianblog.com/media/images/fullsizeoutput_28a.width-1600.jpg','content':'Paris is the capital and most populous city of France, with an estimated population of 2,148,271 residents as of 2020,'},
        {'name':'Maldives','image':'https://qtxasset.com/Hotel%20Management-1508949476/mercuremaldiveskoodooresortmaldivesexterior.jpg?zbxIScOeOJEvMU8kcH1pm8g7UG9zZegb','content':'Maldives officially the Republic of Maldives, is a small archipelagic island country in South Asia, situated in the Arabian Sea of the Indian Ocean'},
        {'name':'Bangkok','image':'https://www.smartertravel.com/uploads/2017/02/bangkok-grand-palace-at-night-1200x627.jpg','content':'Bangkok is the capital and most populous city of Thailand. It is known in Thai as Krung Thep Maha Nakhon or simply Krung Thep.'},
        {'name':'Greece','image':'https://cdn1.matadornetwork.com/blogs/1/2011/05/greece-1200x853.jpg','content':'Greece officially the Hellenic Republic, also known as Hellas, is a country located in Southeast Europe.'},
        {'name':'London','image':'https://imageproxy.themaven.net//https%3A%2F%2Fwww.history.com%2F.image%2FMTYyNDg1MjE3MTI1Mjc5Mzk4%2Ftopic-london-gettyimages-760251843-promo.jpg','content':'London is the capital and largest city of England and the United Kingdom. The city stands on the River Thames in the south-east of England'}
],
    mall:[
        {'name':'Kamppi Mall','image':'https://www.likealocalguide.com/media/cache/e8/f9/e8f98480d184719a81904c6289333f18.jpg'},
        {'name':'Forum Mall','image':'https://media-cdn.tripadvisor.com/media/photo-s/05/4b/81/f4/the-forum.jpg'},
        {'name':'Mall of Tripla','image':'https://www.tohology.com/images/cms/data/Travel-stories/Events/2019/2019-10-17-The_Mall_of_Tripla/The_Mall_of_Tripla_in_Helsinki_7.jpg'},
        {'name':'Jumbo Mall','image':'https://www.thegalleria.ae/media/images/Jumbo-Electroincs-Galleria-M.2e16d0ba.fill-1920x1000-c50.jpg'},
        {'name':'Redi Mall','image':'https://newsnowfinland.fi/wp-content/uploads/2018/09/IMG_20180919_152046.jpg'}
],
    shop:[
        {'name':'H&M','image':'https://www.austrianblog.com/media/images/fullsizeoutput_28a.width-1600.jpg'},
        {'name':'Stockman','image':'https://qtxasset.com/Hotel%20Management-1508949476/mercuremaldiveskoodooresortmaldivesexterior.jpg?zbxIScOeOJEvMU8kcH1pm8g7UG9zZegb'},
        {'name':'Jack & Jones','image':'https://www.smartertravel.com/uploads/2017/02/bangkok-grand-palace-at-night-1200x627.jpg'},
        {'name':'Merimekko','image':'https://cdn1.matadornetwork.com/blogs/1/2011/05/greece-1200x853.jpg'},
        {'name':'Dressman','image':'https://imageproxy.themaven.net//https%3A%2F%2Fwww.history.com%2F.image%2FMTYyNDg1MjE3MTI1Mjc5Mzk4%2Ftopic-london-gettyimages-760251843-promo.jpg',}
],
    basket:[],
    user:null
}


const reducer = (state,action)=>{
    console.log(action);
    switch(action.type){
        case 'List_CITY':
            return{
                ...state,
                cities: action.cities
            }
        case 'List_Mall':
            return{
                ...state,
                cities: action.cities
            }
        case 'search':
            return{
                ...state,
                search: action.search
            }
        default:
          return state;
    }
}
export default reducer;
