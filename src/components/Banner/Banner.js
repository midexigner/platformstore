import React from 'react'
import './Banner.scss'

const Banner = ({image,title,children}) => {
    return (
        <div className="banner-section text-left" style={{            backgroundImage:`url(${image})`}}>
           <div className="breadcrumb-bg breadcrumb-bg-about py-sm-5 py-4">
           <div className="container py-2">
           {title? ( <h2 className="title">{title}</h2>):null }
           
        </div>
        <div className="container py-2">
        {children}
        </div>
        </div>
           </div>
    )
}

export default Banner