import React from 'react'
import './Title.scss'

const Title = ({title,subtitle}) => {
    return (
        <div className='title-content text-left mb-lg-5 mb-4'>
            {subtitle? ( <h6 className="sub-title">{subtitle}</h6>):null }
            {title? ( <h3 className="hny-title">{title}</h3>):null }
        </div>
    )
}

export default Title
