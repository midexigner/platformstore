import React from 'react'
import { Link } from 'react-router-dom'
import './ProductCard.scss'

const ProductCard = ({title,desc,url,img,isDetailed}) => {
    return (
      
        <div className={` mb-4 col-lg-4 col-md-4 col-6 ${isDetailed && "col-lg-12 col-md-12 col-12"}`}>
        <div className="subject-card-header column">
          {url && !isDetailed ?(
            <Link to={url} className="card_title p-lg-4 d-block">
          {img? ( <img src={img} className={`img-fluid img-card ${isDetailed && "img-Large"}`} alt={title} />):null }
            <div className={`subject-content mt-4 ${isDetailed && "col-lg-6 col-md-6 col-6"}`}>
            {title? (<h4>{title}</h4>):null }
            {desc? (<p>{desc}</p>):null }
            </div>
             
              </Link>
          ):null}

{isDetailed ?(
         <div className="row m-4 p-4">
        <div className="col-lg-6 col-md-6 col-6">
        {img? ( <img src={img} className={`img-fluid img-card ${isDetailed && "img-Large"}`} alt={title} />):null }
        </div>
            <div className={`col-lg-6 col-md-6 col-6`}>
            {title? (<h4>{title}</h4>):null }
            {desc? (<p>{desc}</p>):null }
            </div>
         </div>
          ):null}
       
        </div>
        </div>
    )
}

export default ProductCard
