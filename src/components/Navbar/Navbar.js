import React from 'react'
import { Link } from 'react-router-dom'

const Navbar = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <div className="container">
                <div className="row">
                <Link className="navbar-brand" to="/">Store</Link>
                </div>

            </div>
        </nav>

    )
}

export default Navbar
